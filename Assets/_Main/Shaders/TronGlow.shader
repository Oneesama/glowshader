// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/TronGlow"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,0)
		_Albedo("Albedo", 2D) = "white" {}
		_NormalMap("NormalMap", 2D) = "bump" {}
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		[HDR]_GlowColor("GlowColor", Color) = (0.4392157,11.98431,0,0)
		_GlowSize("GlowSize", Range( 0 , 10)) = 4
		_EdgeDensity("EdgeDensity", Range( 0 , 1)) = 0.2
		_GlowBoost("GlowBoost", Range( 1 , 10)) = 5
		_PulseSpeed("PulseSpeed", Range( 0 , 50)) = 2
		_PulseTimeOffset("PulseTimeOffset", Range( 0 , 1)) = 0
		_MinPulse("MinPulse", Range( 0 , 1)) = 0.3
		_MaxPulse("MaxPulse", Range( 0 , 1)) = 1
		_GlowStrength("GlowStrength", Range( 0 , 1)) = 1
		_GlowToAlbedoBlend("GlowToAlbedoBlend", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _NormalMap;
		uniform float4 _NormalMap_ST;
		uniform float4 _Color;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float _GlowSize;
		uniform float _EdgeDensity;
		uniform float4 _GlowColor;
		uniform float _GlowBoost;
		uniform float _PulseSpeed;
		uniform float _PulseTimeOffset;
		uniform float _MinPulse;
		uniform float _MaxPulse;
		uniform float _GlowStrength;
		uniform float4 _GlowToAlbedoBlend;
		uniform float _Metallic;
		uniform float _Smoothness;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_NormalMap = i.uv_texcoord * _NormalMap_ST.xy + _NormalMap_ST.zw;
			o.Normal = UnpackNormal( tex2D( _NormalMap, uv_NormalMap ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float lerpResult50 = lerp( ( pow( ( 1.0 - sin( ( (i.uv_texcoord).x * 3.1416 ) ) ) , _GlowSize ) - _EdgeDensity ) , ( pow( ( 1.0 - sin( ( (i.uv_texcoord).y * 3.1416 ) ) ) , _GlowSize ) - _EdgeDensity ) , 0.5);
			float clampResult59 = clamp( lerpResult50 , 0.0 , 100000.0 );
			float mulTime63 = _Time.y * ( _PulseSpeed + _PulseTimeOffset );
			float4 temp_output_54_0 = ( ( ( ( clampResult59 * _GlowColor ) * _GlowBoost ) * (_MinPulse + (( ( sin( mulTime63 ) + 1.0 ) / 2.0 ) - 0.0) * (_MaxPulse - _MinPulse) / (1.0 - 0.0)) ) * _GlowStrength );
			float4 lerpResult89 = lerp( ( _Color * tex2D( _Albedo, uv_Albedo ) ) , temp_output_54_0 , _GlowToAlbedoBlend);
			o.Albedo = lerpResult89.rgb;
			o.Emission = temp_output_54_0.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16700
691;73;837;578;2302.927;849.1909;2.876542;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;27;-5595.769,-141.4655;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;30;-5323.687,-242.775;Float;True;True;False;False;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;34;-5314.888,-15.07066;Float;True;False;True;False;False;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-5023.044,-9.670387;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;3.1416;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-5010.047,-260.5629;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;3.1416;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;37;-4734.963,-269.0971;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;38;-4741.216,13.05233;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-3188.058,767.946;Float;False;Property;_PulseTimeOffset;PulseTimeOffset;10;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-4341.392,-60.02258;Float;False;Property;_GlowSize;GlowSize;6;0;Create;True;0;0;False;0;4;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;42;-4486.165,-269.2741;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-3197.518,672.4407;Float;False;Property;_PulseSpeed;PulseSpeed;9;0;Create;True;0;0;False;0;2;1;0;50;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;43;-4487.64,18.94673;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;49;-4028.13,-3.825356;Float;False;Property;_EdgeDensity;EdgeDensity;7;0;Create;True;0;0;False;0;0.2;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;72;-2897.898,673.4427;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;46;-4055.086,96.79608;Float;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;45;-4064.318,-229.14;Float;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;47;-3684.265,99.71466;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;48;-3697.218,-228.911;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;63;-2765.807,673.3578;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;50;-3391.823,-93.11649;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;64;-2562.489,673.8109;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;65;-2355.697,684.9824;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;52;-3340.607,302.8673;Float;False;Property;_GlowColor;GlowColor;5;1;[HDR];Create;True;0;0;False;0;0.4392157,11.98431,0,0;0,0.9804158,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;59;-3108.05,-65.8733;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;100000;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-2956.143,89.74157;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;60;-2868.992,277.2154;Float;False;Property;_GlowBoost;GlowBoost;8;0;Create;True;0;0;False;0;5;0;1;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;66;-2227.195,688.6813;Float;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;68;-2549.555,971.1716;Float;False;Property;_MaxPulse;MaxPulse;12;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;67;-2549.476,881.31;Float;False;Property;_MinPulse;MinPulse;11;0;Create;True;0;0;False;0;0.3;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;69;-2081.802,691.4899;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-2541.851,103.3558;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;70;-1923.329,512.9053;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-1865.617,785.9141;Float;False;Property;_GlowStrength;GlowStrength;13;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;76;-1247.458,-426.6936;Float;False;Property;_Color;Color;0;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;77;-1274.753,-208.6758;Float;True;Property;_Albedo;Albedo;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-1414.068,446.566;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;92;-1279.082,0.2592087;Float;False;Property;_GlowToAlbedoBlend;GlowToAlbedoBlend;14;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-924.1048,-318.9864;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-397.0456,418.0668;Float;False;Property;_Smoothness;Smoothness;4;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;89;-850.1224,-64.9725;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0.9245283,0.9245283,0.9245283,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-399.7965,335.5446;Float;False;Property;_Metallic;Metallic;3;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;83;-512.7427,19.8512;Float;True;Property;_NormalMap;NormalMap;2;0;Create;True;0;0;False;0;None;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Custom/TronGlow;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;30;0;27;0
WireConnection;34;0;27;0
WireConnection;36;0;34;0
WireConnection;35;0;30;0
WireConnection;37;0;35;0
WireConnection;38;0;36;0
WireConnection;42;0;37;0
WireConnection;43;0;38;0
WireConnection;72;0;62;0
WireConnection;72;1;71;0
WireConnection;46;0;43;0
WireConnection;46;1;44;0
WireConnection;45;0;42;0
WireConnection;45;1;44;0
WireConnection;47;0;46;0
WireConnection;47;1;49;0
WireConnection;48;0;45;0
WireConnection;48;1;49;0
WireConnection;63;0;72;0
WireConnection;50;0;48;0
WireConnection;50;1;47;0
WireConnection;64;0;63;0
WireConnection;65;0;64;0
WireConnection;59;0;50;0
WireConnection;51;0;59;0
WireConnection;51;1;52;0
WireConnection;66;0;65;0
WireConnection;69;0;66;0
WireConnection;69;3;67;0
WireConnection;69;4;68;0
WireConnection;61;0;51;0
WireConnection;61;1;60;0
WireConnection;70;0;61;0
WireConnection;70;1;69;0
WireConnection;54;0;70;0
WireConnection;54;1;53;0
WireConnection;78;0;76;0
WireConnection;78;1;77;0
WireConnection;89;0;78;0
WireConnection;89;1;54;0
WireConnection;89;2;92;0
WireConnection;0;0;89;0
WireConnection;0;1;83;0
WireConnection;0;2;54;0
WireConnection;0;3;19;0
WireConnection;0;4;20;0
ASEEND*/
//CHKSM=8C7E5711BD9F95AAC65A730A9463F6ABCEEB2E33